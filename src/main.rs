use anyhow::Result;
use genpdf::{
    elements::Paragraph,
    fonts::{self, from_files},
    Document,
};

fn main() -> Result<()> {
    let mut doc = Document::new(from_files(
        "./fonts",
        "LiberationSerif",
        Some(fonts::Builtin::Times),
    )?);

    doc.push(Paragraph::new("poggers?"));

    doc.render_to_file("doc.pdf")?;

    Ok(())
}
